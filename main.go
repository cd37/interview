// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.
package main

import (
	"fmt"
)

// RangeList A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
type RangeList struct {
	elements [][2]int
}

// Add a pair
func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if err := rangeList.checkRange(rangeElement); err != nil {
		return err
	}
	i, j := rangeList.bounds(rangeElement)
	if i <= j {
		if rangeElement[0] > rangeList.elements[i][0] {
			rangeElement[0] = rangeList.elements[i][0]
		}
		if rangeList.elements[j][1] > rangeElement[1] {
			rangeElement[1] = rangeList.elements[j][1]
		}
	}
	newElements := append([][2]int{rangeElement}, rangeList.elements[j+1:]...)
	rangeList.elements = append(rangeList.elements[0:i], newElements...)
	return nil
}

// Remove a pair
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if err := rangeList.checkRange(rangeElement); err != nil {
		return err
	}
	i, j := rangeList.bounds(rangeElement)
	var newElements [][2]int
	for k := i; k <= j; k++ {
		if rangeList.elements[k][0] < rangeElement[0] {
			newElements = append(newElements, [2]int{rangeList.elements[k][0], rangeElement[0]})
		}
		if rangeElement[1] < rangeList.elements[k][1] {
			newElements = append(newElements, [2]int{rangeElement[1], rangeList.elements[k][1]})
		}
	}
	newElements = append(newElements, rangeList.elements[j+1:]...)
	rangeList.elements = append(rangeList.elements[0:i], newElements...)
	return nil
}

// Print current pairs
func (rangeList *RangeList) Print() error {
	for i, v := range rangeList.elements {
		if i != 0 {
			fmt.Print(" ")
		}
		fmt.Printf("[%d %d)", v[0], v[1])
	}
	fmt.Println()
	return nil
}

func (rangeList *RangeList) checkRange(rangeElement [2]int) error {
	if rangeElement[1] < rangeElement[0] {
		return fmt.Errorf("invalid range elements: %v", rangeElement)
	}
	return nil
}

func (rangeList *RangeList) bounds(rangeElement [2]int) (int, int) {
	length := len(rangeList.elements)
	i, j := 0, length-1

	for _, d := range []int{100, 10, 1} {
		for i+d-1 < length && rangeList.elements[i+d-1][1] < rangeElement[0] {
			i += d
		}
		for j >= d-1 && rangeList.elements[j-d+1][0] > rangeElement[1] {
			j -= d
		}
	}
	return i, j
}

// judgePairSide to pair location
//   p2 < p1: return -1
//   p2 collides p1: return 0
//   p2 > p1: return 1
func judgePairSide(p1, p2 [2]int) int {
	if p2[1] < p1[0] {
		return -1
	}
	if p2[0] > p1[1] {
		return 1
	}
	return 0
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)

	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)

	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)

	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)

	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)

	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)

	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)

	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
